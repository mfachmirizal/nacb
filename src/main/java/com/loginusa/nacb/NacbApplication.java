package com.loginusa.nacb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.loginusa.nacb"})
public class NacbApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacbApplication.class, args);
	}

}
