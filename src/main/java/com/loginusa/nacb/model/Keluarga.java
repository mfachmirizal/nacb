package com.loginusa.nacb.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
public class Keluarga {

	
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name="keluarga_id")
    private String id;
	private Boolean isactive;
	@CreationTimestamp
	private LocalDateTime created;
	private String createdby;
	@UpdateTimestamp
	private LocalDateTime updated;
	private String updateby;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "peserta_id", nullable = false)
	private Peserta peserta;	
	@Enumerated(EnumType.STRING)
	private HubunganKeluarga hubunganKeluarga;
	private String jenis_kelamin;
	private String nama;
	private Date tanggal_lahir;
	public Keluarga() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Keluarga(String id, Boolean isactive, LocalDateTime created, String createdby, LocalDateTime updated,
			String updateby, Peserta peserta, HubunganKeluarga hubunganKeluarga, String jenis_kelamin, String nama,
			Date tanggal_lahir) {
		super();
		this.id = id;
		this.isactive = isactive;
		this.created = created;
		this.createdby = createdby;
		this.updated = updated;
		this.updateby = updateby;
		this.peserta = peserta;
		this.hubunganKeluarga = hubunganKeluarga;
		this.jenis_kelamin = jenis_kelamin;
		this.nama = nama;
		this.tanggal_lahir = tanggal_lahir;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Boolean getIsactive() {
		return isactive;
	}
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public LocalDateTime getUpdated() {
		return updated;
	}
	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}
	public String getUpdateby() {
		return updateby;
	}
	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}
	public Peserta getPeserta() {
		return peserta;
	}
	public void setPeserta(Peserta peserta) {
		this.peserta = peserta;
	}
	public HubunganKeluarga getHubunganKeluarga() {
		return hubunganKeluarga;
	}
	public void setHubunganKeluarga(HubunganKeluarga hubunganKeluarga) {
		this.hubunganKeluarga = hubunganKeluarga;
	}
	public String getJenis_kelamin() {
		return jenis_kelamin;
	}
	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public Date getTanggal_lahir() {
		return tanggal_lahir;
	}
	public void setTanggal_lahir(Date tanggal_lahir) {
		this.tanggal_lahir = tanggal_lahir;
	}	
	
	
	
	

}
