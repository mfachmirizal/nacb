package com.loginusa.nacb.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Peserta {

	
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name="peserta_id")
    private String id;
	private Boolean isactive;
	
	@CreationTimestamp
	private LocalDateTime created;
	private String createdby;
	@UpdateTimestamp
	private LocalDateTime updated;
	private String updateby;
	private String jenis_kelamin;
	private String notas;
	private String nama;
	private Date tanggal_lahir;
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "agama_id", nullable = false)
    private Agama agama;
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "kelompok_id", nullable = false)
	private Kelompok kelompok;
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "cabang_id", nullable = false)
	private Cabang cabang;
	private BigInteger gapok;
	private int jumlah_anak;
	private int jumlah_istri;
	private int bup;
	private String src_image;
	
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "peserta")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Set<Keluarga> keluarga;

	
	public Peserta() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Peserta(String id, Boolean isactive, LocalDateTime created, String createdby, LocalDateTime updated,
			String updateby, String jenis_kelamin, String notas, String nama, Date tanggal_lahir, Agama agama,
			Kelompok kelompok, Cabang cabang, BigInteger gapok, int jumlah_anak, int jumlah_istri, int bup,
			String src_image, Set<Keluarga> keluarga) {
		super();
		this.id = id;
		this.isactive = isactive;
		this.created = created;
		this.createdby = createdby;
		this.updated = updated;
		this.updateby = updateby;
		this.jenis_kelamin = jenis_kelamin;
		this.notas = notas;
		this.nama = nama;
		this.tanggal_lahir = tanggal_lahir;
		this.agama = agama;
		this.kelompok = kelompok;
		this.cabang = cabang;
		this.gapok = gapok;
		this.jumlah_anak = jumlah_anak;
		this.jumlah_istri = jumlah_istri;
		this.bup = bup;
		this.src_image = src_image;
		this.keluarga = keluarga;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}

	public String getUpdateby() {
		return updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}

	public String getJenis_kelamin() {
		return jenis_kelamin;
	}

	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Date getTanggal_lahir() {
		return tanggal_lahir;
	}

	public void setTanggal_lahir(Date tanggal_lahir) {
		this.tanggal_lahir = tanggal_lahir;
	}

	public Agama getAgama() {
		return agama;
	}

	public void setAgama(Agama agama) {
		this.agama = agama;
	}

	public Kelompok getKelompok() {
		return kelompok;
	}

	public void setKelompok(Kelompok kelompok) {
		this.kelompok = kelompok;
	}

	public Cabang getCabang() {
		return cabang;
	}

	public void setCabang(Cabang cabang) {
		this.cabang = cabang;
	}

	public BigInteger getGapok() {
		return gapok;
	}

	public void setGapok(BigInteger gapok) {
		this.gapok = gapok;
	}

	public int getJumlah_anak() {
		return jumlah_anak;
	}

	public void setJumlah_anak(int jumlah_anak) {
		this.jumlah_anak = jumlah_anak;
	}

	public int getJumlah_istri() {
		return jumlah_istri;
	}

	public void setJumlah_istri(int jumlah_istri) {
		this.jumlah_istri = jumlah_istri;
	}

	public int getBup() {
		return bup;
	}

	public void setBup(int bup) {
		this.bup = bup;
	}

	public String getSrc_image() {
		return src_image;
	}

	public void setSrc_image(String src_image) {
		this.src_image = src_image;
	}

	

	
	
	
	
}
