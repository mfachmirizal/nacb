package com.loginusa.nacb.model.utility;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;



@Entity
public class ScreenSession {

	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name="screen_session_id")
    private String id;
	private Boolean isactive;
	
	@CreationTimestamp
	private LocalDateTime created;
	private String createdby;
	@UpdateTimestamp
	private LocalDateTime updated;
	private String updateby;	 
	private String screenpath;
	private String object_key;
	private String default_value;
	 
	public ScreenSession() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ScreenSession(String id, Boolean isactive, LocalDateTime created, String createdby, LocalDateTime updated,
			String updateby, String screenpath, String object_key, String default_value) {
		super();
		this.id = id;
		this.isactive = isactive;
		this.created = created;
		this.createdby = createdby;
		this.updated = updated;
		this.updateby = updateby;
		this.screenpath = screenpath;
		this.object_key = object_key;
		this.default_value = default_value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}

	public String getUpdateby() {
		return updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}
	public String getObject_key() {
		return object_key;
	}

	public void setObject_key(String object_key) {
		this.object_key = object_key;
	}

	public String getDefault_value() {
		return default_value;
	}

	public void setDefault_value(String default_value) {
		this.default_value = default_value;
	}

	public String getScreenpath() {
		return screenpath;
	}

	public void setScreenpath(String screenpath) {
		this.screenpath = screenpath;
	}	
	
}
