package com.loginusa.nacb.model.utility;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.loginusa.nacb.utility.TypeDatasource;


@Entity
public class GlobalVariable {

	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name="global_variable_id")
    private String id;
	private Boolean isactive;
	
	@CreationTimestamp
	private LocalDateTime created;
	private String createdby;
	@UpdateTimestamp
	private LocalDateTime updated;
	private String updateby;	
	private String nama;
	private String deskripsi;
	private String searchdata;
	private String value;


	@Enumerated(EnumType.STRING)
	private TypeDatasource datasource;

	public GlobalVariable() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GlobalVariable(String id, Boolean isactive, LocalDateTime created, String createdby, LocalDateTime updated,
			String updateby, String nama, String deskripsi, String searchdata, TypeDatasource datasource, String value) {
		super();
		this.id = id;
		this.isactive = isactive;
		this.created = created;
		this.createdby = createdby;
		this.updated = updated;
		this.updateby = updateby;
		this.nama = nama;
		this.deskripsi = deskripsi;
		this.searchdata = searchdata;
		this.datasource = datasource;
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}

	public String getUpdateby() {
		return updateby;
	}

	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getSearchdata() {
		return searchdata;
	}

	public void setSearchdata(String searchdata) {
		this.searchdata = searchdata;
	}

	public TypeDatasource getDatasource() {
		return datasource;
	}

	public void setDatasource(TypeDatasource datasource) {
		this.datasource = datasource;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
