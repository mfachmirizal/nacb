package com.loginusa.nacb.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class Agama {

	
	@Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name="agama_id")
    private String id;
	private Boolean isactive;
	
	@CreationTimestamp
	private LocalDateTime created;
	private String createdby;
	@UpdateTimestamp
	private LocalDateTime updated;
	private String updateby;	
	private String kode;
	private String nama;
	private String deskripsi;
	
	
	
	public Agama() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Agama(String id, Boolean isactive, LocalDateTime created, String createdby, LocalDateTime updated,
			String updateby, String kode, String nama, String deskripsi) {
		super();
		this.id = id;
		this.isactive = isactive;
		this.created = created;
		this.createdby = createdby;
		this.updated = updated;
		this.updateby = updateby;
		this.kode = kode;
		this.nama = nama;
		this.deskripsi = deskripsi;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public Boolean getIsactive() {
		return isactive;
	}


	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}


	public LocalDateTime getCreated() {
		return created;
	}


	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public String getCreatedby() {
		return createdby;
	}


	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}


	public LocalDateTime getUpdated() {
		return updated;
	}


	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}


	public String getUpdateby() {
		return updateby;
	}


	public void setUpdateby(String updateby) {
		this.updateby = updateby;
	}


	public String getKode() {
		return kode;
	}


	public void setKode(String kode) {
		this.kode = kode;
	}



	public String getNama() {
		return nama;
	}


	public void setNama(String nama) {
		this.nama = nama;
	}


	public String getDeskripsi() {
		return deskripsi;
	}


	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	

	
}
