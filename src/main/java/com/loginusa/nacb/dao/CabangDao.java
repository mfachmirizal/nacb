package com.loginusa.nacb.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loginusa.nacb.model.Cabang;
import com.loginusa.nacb.repository.CabangRepository;
import com.loginusa.nacb.service.CabangService;

@Service
public class CabangDao implements CabangService {

	
	@Autowired
	private CabangRepository cabangRepository;
	
	@Override
	public Iterable<Cabang> FindAll() {
		return cabangRepository.findAll();
	}

	@Override
	public Cabang FindById(String id) {
		 Optional<Cabang> cabang = cabangRepository.findById(id);
		 return cabang.get();
	}

	@Override
	public Cabang Save(Cabang cabang) {
		return cabangRepository.save(cabang);
	}

	@Override
	public void Delete(Cabang cabang) {
		cabangRepository.delete(cabang);
		
	}

	@Override
	public List<Cabang> GetCabang() {
		return (List<Cabang>) cabangRepository.findAll();
	}

	@Override
	public Page<Cabang> getPaginatedCabang(Pageable pageable) {
		return cabangRepository.findAll(pageable);
	}

}
