package com.loginusa.nacb.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loginusa.nacb.model.Keluarga;
import com.loginusa.nacb.repository.KeluargaRepository;
import com.loginusa.nacb.service.KeluargaService;

@Service
public class KeluargaDao implements KeluargaService {

	
	@Autowired
	private KeluargaRepository keluargaRepository;
	
	@Override
	public Iterable<Keluarga> FindAll() {
		return keluargaRepository.findAll();
	}

	@Override
	public Keluarga FindById(String id) {
		 Optional<Keluarga> keluarga = keluargaRepository.findById(id);
		 return keluarga.get();
	}

	@Override
	public Keluarga Save(Keluarga keluarga) {
		return keluargaRepository.save(keluarga);
	}

	@Override
	public void Delete(Keluarga keluarga) {
		keluargaRepository.delete(keluarga);
		
	}

	@Override
	public List<Keluarga> GetKeluarga() {
		return (List<Keluarga>) keluargaRepository.findAll();
	}

	@Override
	public Page<Keluarga> getPaginatedKeluarga(Pageable pageable) {
		return keluargaRepository.findAll(pageable);
	}

	/*@Override
	public List<Keluarga> GetKeluargaByPeserta(Peserta peserta) {
		List<Keluarga> result = null;
		List<Keluarga> keluarga = new ArrayList<>();
        for (Keluarga temp : keluarga) {
            if (peserta==temp.getPeserta()) {
            	result.add(temp);
            }
        }
		return result;
	}*/

}
