package com.loginusa.nacb.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loginusa.nacb.model.Kelompok;
import com.loginusa.nacb.repository.KelompokRepository;
import com.loginusa.nacb.service.KelompokService;

@Service
public class KelompokDao implements KelompokService {

	
	@Autowired
	private KelompokRepository kelompokRepository;
	
	@Override
	public Iterable<Kelompok> FindAll() {
		return kelompokRepository.findAll();
	}

	@Override
	public Kelompok FindById(String id) {
		 Optional<Kelompok> kelompok = kelompokRepository.findById(id);
		 return kelompok.get();
	}

	@Override
	public Kelompok Save(Kelompok kelompok) {
		return kelompokRepository.save(kelompok);
	}

	@Override
	public void Delete(Kelompok kelompok) {
		kelompokRepository.delete(kelompok);
		
	}

	@Override
	public List<Kelompok> GetKelompok() {
		return (List<Kelompok>) kelompokRepository.findAll();
	}

	@Override
	public Page<Kelompok> getPaginatedKelompok(Pageable pageable) {
		return kelompokRepository.findAll(pageable);
	}

}
