package com.loginusa.nacb.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loginusa.nacb.model.Peserta;
import com.loginusa.nacb.repository.PesertaRepository;
import com.loginusa.nacb.service.PesertaService;

@Service
public class PesertaDao implements PesertaService {

	
	@Autowired
	private PesertaRepository pesertaRepository;
	
	@Override
	public Iterable<Peserta> FindAll() {
		return pesertaRepository.findAll();
	}

	@Override
	public Peserta FindById(String id) {
		 Optional<Peserta> peserta = pesertaRepository.findById(id);
		 return peserta.get();
	}

	@Override
	public Peserta Save(Peserta peserta) {
		return pesertaRepository.save(peserta);
	}

	@Override
	public void Delete(Peserta peserta) {
		pesertaRepository.delete(peserta);
		
	}

	@Override
	public List<Peserta> GetPeserta() {
		return (List<Peserta>) pesertaRepository.findAll();
	}

	@Override
	public Page<Peserta> getPaginatedPeserta(Pageable pageable) {
		return pesertaRepository.findAll(pageable);
	}

}
