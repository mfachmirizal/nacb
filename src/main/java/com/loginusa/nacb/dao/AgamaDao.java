package com.loginusa.nacb.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loginusa.nacb.model.Agama;
import com.loginusa.nacb.repository.AgamaRepository;
import com.loginusa.nacb.service.AgamaService;

@Service
public class AgamaDao implements AgamaService {

	
	@Autowired
	private AgamaRepository agamaRepository;
	
	@Override
	public Iterable<Agama> FindAll() {
		return agamaRepository.findAll();
	}

	@Override
	public Agama FindById(String id) {
		 Optional<Agama> agama = agamaRepository.findById(id);
		 return agama.get();
	}

	@Override
	public Agama Save(Agama agama) {
		return agamaRepository.save(agama);
	}

	@Override
	public void Delete(Agama agama) {
		agamaRepository.delete(agama);
		
	}

	@Override
	public List<Agama> GetAgama() {
		return (List<Agama>) agamaRepository.findAll();
	}

	@Override
	public Page<Agama> getPaginatedAgama(Pageable pageable) {
		return agamaRepository.findAll(pageable);
	}

}
