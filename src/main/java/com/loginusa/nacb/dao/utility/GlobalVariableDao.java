package com.loginusa.nacb.dao.utility;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loginusa.nacb.model.utility.GlobalVariable;
import com.loginusa.nacb.repository.utility.GlobalVariableRepository;
import com.loginusa.nacb.service.utility.GlobalVariableService;

@Service
public class GlobalVariableDao implements GlobalVariableService {
	
	@Autowired
	private GlobalVariableRepository globalVariableRepository;

	@Override
	public Iterable<GlobalVariable> FindAll() {
		return globalVariableRepository.findAll();
	}

	@Override
	public GlobalVariable FindById(String id) {
		 Optional<GlobalVariable> peserta = globalVariableRepository.findById(id);
		 return peserta.get();
	}

	@Override
	public GlobalVariable Save(GlobalVariable peserta) {
		return globalVariableRepository.save(peserta);
	}

	@Override
	public void Delete(GlobalVariable peserta) {
		globalVariableRepository.delete(peserta);
		
	}

	@Override
	public List<GlobalVariable> GetGlobalVariable() {
		return (List<GlobalVariable>) globalVariableRepository.findAll();
	}

	@Override
	public Page<GlobalVariable> getPaginatedGlobalVariable(Pageable pageable) {
		return globalVariableRepository.findAll(pageable);
	}

	@Override
	public GlobalVariable findByNama(String nama) {
		// TODO Auto-generated method stub
		return globalVariableRepository.findByNama(nama);
	}
	
	

}
