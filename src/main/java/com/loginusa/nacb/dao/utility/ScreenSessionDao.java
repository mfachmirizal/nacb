package com.loginusa.nacb.dao.utility;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.loginusa.nacb.model.utility.GlobalVariable;
import com.loginusa.nacb.model.utility.ScreenSession;
import com.loginusa.nacb.repository.utility.ScreenSessionRepository;
import com.loginusa.nacb.service.utility.ScreenSessionService;


@Service
public class ScreenSessionDao implements ScreenSessionService {
	
	@Autowired
	private ScreenSessionRepository screenSessionRepository;

	@Override
	public Iterable<ScreenSession> FindAll() {
		return screenSessionRepository.findAll();
	}

	@Override
	public ScreenSession FindById(String id) {
		 Optional<ScreenSession> peserta = screenSessionRepository.findById(id);
		 return peserta.get();
	}

	@Override
	public ScreenSession Save(ScreenSession peserta) {
		return screenSessionRepository.save(peserta);
	}

	@Override
	public void Delete(ScreenSession peserta) {
		screenSessionRepository.delete(peserta);
		
	}

	@Override
	public List<ScreenSession> GetScreenSession() {
		return (List<ScreenSession>) screenSessionRepository.findAll();
	}

	@Override
	public Page<ScreenSession> getPaginatedScreenSession(Pageable pageable) {
		return screenSessionRepository.findAll(pageable);
	}
	
	
	@Override
	public List<ScreenSession> findByScreenpath(String screenpath) {
		// TODO Auto-generated method stub
		return (List<ScreenSession>) screenSessionRepository.findByScreenpath(screenpath);
	}
	

}
