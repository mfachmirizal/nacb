package com.loginusa.nacb.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.loginusa.nacb.model.Peserta;

@Component
public interface PesertaService {
	
 Iterable<Peserta> FindAll();

 Peserta FindById(String id);

 Peserta Save(Peserta agama);

 void Delete(Peserta agama);
 
 List<Peserta> GetPeserta();
 
 Page<Peserta> getPaginatedPeserta(Pageable pageable);
 


}
