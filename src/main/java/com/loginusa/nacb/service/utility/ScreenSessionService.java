package com.loginusa.nacb.service.utility;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
 
import com.loginusa.nacb.model.utility.ScreenSession;

@Component
public interface ScreenSessionService {
	
 Iterable<ScreenSession> FindAll();

 ScreenSession FindById(String id);
//
 List<ScreenSession> findByScreenpath(String screenpath);
 
 ScreenSession Save(ScreenSession globalvariable);

 void Delete(ScreenSession globalvariable);
 
 List<ScreenSession> GetScreenSession();
 
 Page<ScreenSession> getPaginatedScreenSession(Pageable pageable);

 
}
