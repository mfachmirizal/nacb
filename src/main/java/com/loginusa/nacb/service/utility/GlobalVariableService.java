package com.loginusa.nacb.service.utility;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.loginusa.nacb.model.utility.GlobalVariable;

@Component
public interface GlobalVariableService {
	
 Iterable<GlobalVariable> FindAll();

 GlobalVariable FindById(String id);
 
 GlobalVariable findByNama(String nama);

 GlobalVariable Save(GlobalVariable globalvariable);

 void Delete(GlobalVariable globalvariable);
 
 List<GlobalVariable> GetGlobalVariable();
 
 Page<GlobalVariable> getPaginatedGlobalVariable(Pageable pageable);
 

}
