package com.loginusa.nacb.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.loginusa.nacb.model.Cabang;
@Component
public interface CabangService {
	
 Iterable<Cabang> FindAll();

 Cabang FindById(String id);

 Cabang Save(Cabang cabang);

 void Delete(Cabang cabang);
 
 List<Cabang> GetCabang();
 
 Page<Cabang> getPaginatedCabang(Pageable pageable);
 


}