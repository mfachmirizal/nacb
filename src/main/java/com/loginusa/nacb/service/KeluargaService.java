package com.loginusa.nacb.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.loginusa.nacb.model.Keluarga;

@Component
public interface KeluargaService {
	
 Iterable<Keluarga> FindAll();

 Keluarga FindById(String id);

 Keluarga Save(Keluarga agama);

 void Delete(Keluarga agama);
 
 List<Keluarga> GetKeluarga();
 
 //List<Keluarga> GetKeluargaByPeserta(Peserta peserta);
 
 Page<Keluarga> getPaginatedKeluarga(Pageable pageable);
 


}
