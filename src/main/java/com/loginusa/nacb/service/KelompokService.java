package com.loginusa.nacb.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.loginusa.nacb.model.Kelompok;
@Component
public interface KelompokService {
	
 Iterable<Kelompok> FindAll();

 Kelompok FindById(String id);

 Kelompok Save(Kelompok kelompok);

 void Delete(Kelompok kelompok);
 
 List<Kelompok> GetKelompok();
 
 Page<Kelompok> getPaginatedKelompok(Pageable pageable);
 


}
