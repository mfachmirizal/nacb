package com.loginusa.nacb.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.loginusa.nacb.model.Agama;
@Component
public interface AgamaService {
	
 Iterable<Agama> FindAll();

 Agama FindById(String id);

 Agama Save(Agama agama);

 void Delete(Agama agama);
 
 List<Agama> GetAgama();
 
 Page<Agama> getPaginatedAgama(Pageable pageable);
 


}
