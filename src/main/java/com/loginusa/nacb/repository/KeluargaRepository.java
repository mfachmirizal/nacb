package com.loginusa.nacb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loginusa.nacb.model.Keluarga;


@Repository
public interface KeluargaRepository 	
		extends PagingAndSortingRepository<Keluarga,String>, CrudRepository<Keluarga, String>{
	
			Page<Keluarga> findAll(Pageable pageable);
			

}
