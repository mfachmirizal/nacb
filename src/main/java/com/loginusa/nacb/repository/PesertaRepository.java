package com.loginusa.nacb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loginusa.nacb.model.Peserta;


@Repository
public interface PesertaRepository 	
		extends PagingAndSortingRepository<Peserta,String>, CrudRepository<Peserta, String>{
	
			Page<Peserta> findAll(Pageable pageable);
			

}
