package com.loginusa.nacb.repository.utility;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
 
import com.loginusa.nacb.model.utility.ScreenSession;
 

@Repository
public interface ScreenSessionRepository 	
		extends PagingAndSortingRepository<ScreenSession,String>, CrudRepository<ScreenSession, String>{
			Page<ScreenSession> findAll(Pageable pageable);			 
			
			List<ScreenSession> findByScreenpath(String screenpath);
}
