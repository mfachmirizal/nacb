package com.loginusa.nacb.repository.utility;
 

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository; 
import com.loginusa.nacb.model.utility.GlobalVariable;
 

@Repository
public interface GlobalVariableRepository 	
		extends PagingAndSortingRepository<GlobalVariable,String>, CrudRepository<GlobalVariable, String>{
			Page<GlobalVariable> findAll(Pageable pageable);
			
			GlobalVariable findByNama(String nama); 
}
