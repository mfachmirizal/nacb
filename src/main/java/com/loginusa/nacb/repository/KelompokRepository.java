package com.loginusa.nacb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loginusa.nacb.model.Kelompok;


@Repository
public interface KelompokRepository 	
		extends PagingAndSortingRepository<Kelompok,String>, CrudRepository<Kelompok, String>{
	
			Page<Kelompok> findAll(Pageable pageable);
			

}
