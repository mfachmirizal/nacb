package com.loginusa.nacb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loginusa.nacb.model.Cabang;


@Repository
public interface CabangRepository 		
	extends PagingAndSortingRepository<Cabang,String>, CrudRepository<Cabang, String>{
	
	Page<Cabang> findAll(Pageable pageable);
	

}

