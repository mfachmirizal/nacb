package com.loginusa.nacb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.loginusa.nacb.model.Agama;


@Repository
public interface AgamaRepository 	
		extends PagingAndSortingRepository<Agama,String>, CrudRepository<Agama, String>{
	
			Page<Agama> findAll(Pageable pageable);
}
