package com.loginusa.nacb.controller.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
 
import com.loginusa.nacb.model.utility.GlobalVariable;
import com.loginusa.nacb.service.utility.GlobalVariableService;
 

@RestController
@RequestMapping(value="/ref/globalvariable")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class GlobalVariableController {

	@Autowired
	private GlobalVariableService globalvariableService;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public GlobalVariable createGlobalVariable(@Valid @RequestBody GlobalVariable globalvariable) {
		
		return globalvariableService.Save(globalvariable);
		
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public List<GlobalVariable> getGlobalVariable() {
        return  globalvariableService.GetGlobalVariable();
    }	
	
    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<GlobalVariable> getGlobalVariableById(@PathVariable(value="id") String id) {
        GlobalVariable globalvariable = globalvariableService.FindById(id);
        
        if (globalvariable==null) {
        	
        	return ResponseEntity.notFound().build();
        }
    	
    	return ResponseEntity.ok().body(globalvariable);
 
    }
       
    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<GlobalVariable> updatetGlobalVariableById(@PathVariable(value="id") String id,@Valid @RequestBody GlobalVariable gVDetails) {
       
    	GlobalVariable globalVar = globalvariableService.FindById(id);
        
        if (globalVar==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        
        globalVar = gVDetails;
        
        GlobalVariable globalVarUpdate = globalvariableService.Save(globalVar);
    	return ResponseEntity.ok().body(globalVarUpdate);
    }
    
    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<GlobalVariable> deleteGlobalVariableById(@PathVariable(value="id") String id) {
        GlobalVariable globalvariable = globalvariableService.FindById(id);
        
        if (globalvariable==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        globalvariableService.Delete(globalvariable);
    	return ResponseEntity.ok().build();
    }
    
	
	
}
