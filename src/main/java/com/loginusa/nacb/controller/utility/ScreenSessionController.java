package com.loginusa.nacb.controller.utility;
 
import java.util.List; 

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
 
import com.loginusa.nacb.model.utility.ScreenSession;
import com.loginusa.nacb.service.utility.ScreenSessionService;
 

@RestController
@RequestMapping(value="/ref/screensession")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class ScreenSessionController {

	@Autowired
	private ScreenSessionService screensessionService;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public ScreenSession createScreenSession(@Valid @RequestBody ScreenSession screensession) {
		
		return screensessionService.Save(screensession);
		
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public List<ScreenSession> getScreenSession() {
        return  screensessionService.GetScreenSession();
    }	
	
    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<ScreenSession> getScreenSessionById(@PathVariable(value="id") String id) {
        ScreenSession screensession = screensessionService.FindById(id);
        
        if (screensession==null) {
        	
        	return ResponseEntity.notFound().build();
        }
    	
    	return ResponseEntity.ok().body(screensession);
 
    }
       
    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<ScreenSession> updatetScreenSessionById(@PathVariable(value="id") String id,@Valid @RequestBody ScreenSession sSDetails) {
       
    	ScreenSession screenSess = screensessionService.FindById(id);
        
        if (screenSess==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        
        screenSess = sSDetails;
        
        ScreenSession screenSessUpdate = screensessionService.Save(screenSess);
    	return ResponseEntity.ok().body(screenSessUpdate);
    }
    
    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<ScreenSession> deleteScreenSessionById(@PathVariable(value="id") String id) {
        ScreenSession screensession = screensessionService.FindById(id);
        
        if (screensession==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        screensessionService.Delete(screensession);
    	return ResponseEntity.ok().build();
    }
    
	
	
}
