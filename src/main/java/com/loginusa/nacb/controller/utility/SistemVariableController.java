package com.loginusa.nacb.controller.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loginusa.nacb.model.utility.GlobalVariable;
import com.loginusa.nacb.model.utility.ScreenSession;
import com.loginusa.nacb.service.utility.GlobalVariableService;
import com.loginusa.nacb.service.utility.ScreenSessionService;

@RestController
@RequestMapping(value="/function/sistemvariable")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class SistemVariableController {

	private static final Pattern TAG_REGEX = Pattern.compile("@(.+?)@");

	@Autowired
	private GlobalVariableService globalvariableService;
	@Autowired
	private ScreenSessionService screensessionService;


	@RequestMapping(method=RequestMethod.GET)
	public Map<String, String> getGlobalVariable() {
		Map<String, String> variables = retrieveGlobalVariable();
		
		return variables;
	}	

	@RequestMapping(path="/{screenpath}", method=RequestMethod.GET)
	public Map<String, Map<String, String>> getSystemVariable(@PathVariable(value="screenpath") String screenpath) {
		Map<String, String> ssVariables = new HashMap<>();
		List<ScreenSession> screenSessionList = screensessionService.findByScreenpath("/"+screenpath);
		
		for (ScreenSession ss : screenSessionList) {
			ssVariables.put(ss.getObject_key(), ss.getDefault_value());
		}
		
		Map<String, String> globalVariables = retrieveGlobalVariable();
		
		
		Map<String, Map<String, String>> sistemVariables = new HashMap<>();
		sistemVariables.put("screen", ssVariables);
		sistemVariables.put("global", globalVariables);
 
		return sistemVariables; 
	}
	
	private Map <String, String> retrieveGlobalVariable() {
		Map<String, String> variables = new HashMap<>();
		for (GlobalVariable gv : globalvariableService.GetGlobalVariable()) {
			String varName = gv.getNama();
			String varValue = gv.getValue();

			final List<String> tagValues = new ArrayList<String>();

			do {
				Matcher matcher = TAG_REGEX.matcher(varValue);
				while (matcher.find()) {            	
					tagValues.add(matcher.group(1));
				}
				for (String tagVal : tagValues) {
					System.out.println(varName+" "+tagVal);

					GlobalVariable temp = globalvariableService.findByNama(tagVal);
					String nilaiVariable = temp.getValue();
					varValue = varValue.replaceAll("@"+tagVal+"@", "("+nilaiVariable+")");
					
				}
			} while(varValue.contains("@")); 
			variables.put(varName, varValue);
		} 
		return variables;
	}

}
