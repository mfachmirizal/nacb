package com.loginusa.nacb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loginusa.nacb.model.Peserta;
import com.loginusa.nacb.service.PesertaService;

@RestController
@RequestMapping(value="/master/peserta")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class PesertaController {

	@Autowired
	private PesertaService pesertaService;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public Peserta createPeserta(@Valid @RequestBody Peserta peserta) {
		
		return pesertaService.Save(peserta);
		
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public List<Peserta> getPeserta() {
        return  pesertaService.GetPeserta();
    }	
	
    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Peserta> getPesertaById(@PathVariable(value="id") String id) {
        Peserta peserta = pesertaService.FindById(id);
        
        if (peserta==null) {
        	
        	return ResponseEntity.notFound().build();
        }
    	
    	return ResponseEntity.ok().body(peserta);
 
    }
    
    @RequestMapping(path="/dropdowndata",method=RequestMethod.GET)
    public List<Object> getPesertaOnlyIdAndNotas() {
    	List<Object> result = new ArrayList<>(); 
    	for (Peserta peserta : pesertaService.GetPeserta()) {
    		Map<String, Object> temp = new HashMap<String, Object>();
    		temp.put("value", peserta.getId());
    		temp.put("label", peserta.getNotas());
    		result.add(temp);
    	}    	
        return result;
    }	
    
    
//    @RequestMapping
//    public Map<String, Object> getPesertaWithPrperties(@PathVariable("id") long id, String include) {
//
//        Game game = service.loadGame(id);
//        // check the `include` parameter and create a map containing only the required attributes
//        Map<String, Object> gameMap = service.convertGameToMap(game, include);
//
//        return gameMap;
//
//    }
    
    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Peserta> updatetPesertaById(@PathVariable(value="id") String id,@Valid @RequestBody Peserta pstDetails) {
       
    	Peserta pst = pesertaService.FindById(id);
        
        if (pst==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        
        pst.setNama(pstDetails.getNama());
        pst.setCabang(pstDetails.getCabang());
        pst.setAgama(pstDetails.getAgama());
        
        
        Peserta pstUpdate = pesertaService.Save(pst);
    	return ResponseEntity.ok().body(pstUpdate);
    }
    
    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Peserta> deletePesertaById(@PathVariable(value="id") String id) {
        Peserta peserta = pesertaService.FindById(id);
        
        if (peserta==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        pesertaService.Delete(peserta);
    	return ResponseEntity.ok().build();
    }
    
	
	
}
