package com.loginusa.nacb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loginusa.nacb.model.Keluarga;
import com.loginusa.nacb.service.KeluargaService;

@RestController
@RequestMapping(value="/master/keluarga")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class KeluargaController {

	@Autowired
	private KeluargaService keluargaService;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public Keluarga createKeluarga(@Valid @RequestBody Keluarga keluarga) {
		
		return keluargaService.Save(keluarga);
		
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public List<Keluarga> getKeluarga() {
        return  keluargaService.GetKeluarga();
    }	

       
      
	
    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Keluarga> getKeluargaById(@PathVariable(value="id") String id) {
        Keluarga keluarga = keluargaService.FindById(id);
        
        if (keluarga==null) {
        	
        	return ResponseEntity.notFound().build();
        }
    	
    	return ResponseEntity.ok().body(keluarga);
 
    }
    
    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Keluarga> updatetKeluargaById(@PathVariable(value="id") String id,@Valid @RequestBody Keluarga pstDetails) {
       
    	Keluarga pst = keluargaService.FindById(id);
        
        if (pst==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        
        pst.setNama(pstDetails.getNama());
        
        Keluarga pstUpdate = keluargaService.Save(pst);
    	return ResponseEntity.ok().body(pstUpdate);
    }
    
    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Keluarga> deleteKeluargaById(@PathVariable(value="id") String id) {
        Keluarga keluarga = keluargaService.FindById(id);
        
        if (keluarga==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        keluargaService.Delete(keluarga);
    	return ResponseEntity.ok().build();
    }
    
	
	
}
