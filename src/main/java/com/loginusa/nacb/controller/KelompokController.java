package com.loginusa.nacb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loginusa.nacb.model.Kelompok;
import com.loginusa.nacb.service.KelompokService;

@RestController
@RequestMapping(value="/ref/kelompok")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class KelompokController {

	@Autowired
	private KelompokService kelompokService;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public Kelompok createKelompok(@Valid @RequestBody Kelompok kelompok) {
		
		return kelompokService.Save(kelompok);
		
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public List<Kelompok> getKelompok() {
        return  kelompokService.GetKelompok();
    }	
	
    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Kelompok> getKelompokById(@PathVariable(value="id") String id) {
        Kelompok kelompok = kelompokService.FindById(id);
        
        if (kelompok==null) {
        	
        	return ResponseEntity.notFound().build();
        }
    	
    	return ResponseEntity.ok().body(kelompok);
    }
    
    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Kelompok> updatetKelompokById(@PathVariable(value="id") String id,@Valid @RequestBody Kelompok klpDetails) {
       
    	Kelompok klp = kelompokService.FindById(id);
        
        if (klp==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        
        klp.setNama(klpDetails.getNama());
        klp.setDeskripsi(klpDetails.getDeskripsi());
        
        Kelompok klpUpdate = kelompokService.Save(klp);
    	return ResponseEntity.ok().body(klpUpdate);
    }
    
    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Kelompok> deleteKelompokById(@PathVariable(value="id") String id) {
        Kelompok kelompok = kelompokService.FindById(id);
        
        if (kelompok==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        kelompokService.Delete(kelompok);
    	return ResponseEntity.ok().build();
    }
    
	
	
}
