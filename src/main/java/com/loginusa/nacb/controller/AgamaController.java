package com.loginusa.nacb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loginusa.nacb.model.Agama;
import com.loginusa.nacb.service.AgamaService;

@RestController
@RequestMapping(value="/ref/agama")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class AgamaController {

	@Autowired
	private AgamaService agamaService;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public Agama createAgama(@Valid @RequestBody Agama agama) {
		
		return agamaService.Save(agama);
		
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public List<Agama> getAgama() {
        return  agamaService.GetAgama();
    }	
	
    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Agama> getAgamaById(@PathVariable(value="id") String id) {
        Agama agama = agamaService.FindById(id);
        
        if (agama==null) {
        	
        	return ResponseEntity.notFound().build();
        }
    	
    	return ResponseEntity.ok().body(agama);
    }
    
    
    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Agama> updatetAgamaById(@PathVariable(value="id") String id,@Valid @RequestBody Agama agmDetails) {
       
    	Agama agm = agamaService.FindById(id);
        
        if (agm==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        
        agm.setNama(agmDetails.getNama());
        agm.setDeskripsi(agmDetails.getDeskripsi());
        
        Agama agmUpdate = agamaService.Save(agm);
    	return ResponseEntity.ok().body(agmUpdate);
    }
    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Agama> deleteAgamaById(@PathVariable(value="id") String id) {
        Agama agama = agamaService.FindById(id);
        
        if (agama==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        agamaService.Delete(agama);
    	return ResponseEntity.ok().build();
    }
    
	
	
}
