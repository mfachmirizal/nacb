package com.loginusa.nacb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loginusa.nacb.model.Cabang;
import com.loginusa.nacb.service.CabangService;

@RestController
@RequestMapping(value="/ref/cabang")
@Component
@CrossOrigin(origins="http://localhost:3000")
public class CabangController {

	@Autowired
	private CabangService cabangService;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public Cabang createCabang(@Valid @RequestBody Cabang cabang) {
		
		return cabangService.Save(cabang);
		
	}
	
    @RequestMapping(method=RequestMethod.GET)
    public List<Cabang> getCabang() {
        return  cabangService.GetCabang();
    }	
	
    @RequestMapping(path="/{id}", method=RequestMethod.GET)
    public ResponseEntity<Cabang> getCabangById(@PathVariable(value="id") String id) {
        Cabang cabang = cabangService.FindById(id);
        
        if (cabang==null) {
        	
        	return ResponseEntity.notFound().build();
        }
    	
    	return ResponseEntity.ok().body(cabang);
    }
    
    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Cabang> updatetCabangById(@PathVariable(value="id") String id,@Valid @RequestBody Cabang cbgDetails) {
       
    	Cabang cbg = cabangService.FindById(id);
        
        if (cbg==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        
        cbg.setNama(cbgDetails.getNama());
        cbg.setDeskripsi(cbgDetails.getDeskripsi());
        
        Cabang cbgUpdate = cabangService.Save(cbg);
    	return ResponseEntity.ok().body(cbgUpdate);
    }
    
    
    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Cabang> deleteCabangById(@PathVariable(value="id") String id) {
        Cabang cabang = cabangService.FindById(id);
        
        if (cabang==null) {
        	
        	return ResponseEntity.notFound().build();
        }
        cabangService.Delete(cabang);
    	return ResponseEntity.ok().build();
    }
    
	
	
}
